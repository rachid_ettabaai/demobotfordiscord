<?php

use Dotenv\Dotenv;
use Discord\Discord;
use React\Http\Browser;
use React\EventLoop\Factory;
use Discord\Parts\Channel\Message;
use Psr\Http\Message\ResponseInterface;

require __DIR__ . "/vendor/autoload.php";

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

$loop = Factory::create();

$browser = new Browser($loop);

$options = ["token" => $_ENV["TOKEN"],"loop" => $loop];

$discord = new Discord($options);

$discord->on('message', function (Message $message, Discord $discord) use ($browser) {
    if (strtolower($message->content) == '!joke') {
        $browser->get($_ENV["API_URL"])->then(function (ResponseInterface $response) use ($message) {
            $joke = json_decode($response->getBody())->value;

            $message->reply($joke);
        });
    }
});

$discord->run();
