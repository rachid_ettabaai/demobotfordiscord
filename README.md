# Brief Projet Discord Bot

## Ètapes pour utiliser ce projet en local

- git clone <https://gitlab.com/rachid_ettabaai/demobotfordiscord>
- cd demobotfordiscord/
- create an .env file from .env.example with data from your discord apps
- composer install
- php index.php
- go to your channel where is your bot and tape !joke and you will see the result
